//
//  LoginViewController.swift
//  FirebaseTest
//
//  Created by Gilbert Osborne on 7/12/16.
//  Copyright © 2016 Gilbert Osborne. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: UIViewController {
    

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Check for logged in user
        if let user = FIRAuth.auth()?.currentUser {
            //user logged in
            
            self.logoutButton.alpha = 1.0
            self.usernameLabel.text = user.email
            
            //print(user.uid)
            
        }else{
            //user not logged in
            
            self.logoutButton.alpha = 0
            self.usernameLabel.text = ""
        }
        
    }
    
    @IBAction func createAccountAction(_ sender: AnyObject) {
        
        if self.emailField.text == "" || self.passwordField.text == "" {
            let alertController = UIAlertController(title: "Oops", message: "Please enter username and password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            FIRAuth.auth()?.createUser(withEmail: self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                
                if error == nil {
                    //no error
                    self.logoutButton.alpha = 1.0
                    self.usernameLabel.text = user!.email
                    self.emailField.text = ""
                    self.passwordField.text = ""
                    
                }else{
                    //is error
                    
                    let alertController = UIAlertController(title: "Oops", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
        }
        
    }
    
    @IBAction func loginAction(_ sender: AnyObject) {
        
        if self.emailField.text == "" || self.passwordField.text == "" {
            let alertController = UIAlertController(title: "Oops", message: "Please enter username and password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            FIRAuth.auth()?.signIn(withEmail: self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                
                if error == nil {
                    //no error
                    self.logoutButton.alpha = 1.0
                    self.usernameLabel.text = user!.email
                    self.emailField.text = ""
                    self.passwordField.text = ""
                    
                }else{
                    //is error
                    
                    let alertController = UIAlertController(title: "Oops", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            })
        }
        
        
    }
    
    @IBAction func logoutAction(_ sender: AnyObject) {
        
        try! FIRAuth.auth()?.signOut()
        
        self.logoutButton.alpha = 0.0
        self.usernameLabel.text = ""
        self.emailField.text = ""
        self.passwordField.text = ""
    }
    

}

//
//  ViewController.swift
//  FirebaseTest
//
//  Created by Gilbert Osborne on 7/10/16.
//  Copyright © 2016 Gilbert Osborne. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class ViewController: UIViewController {

    //SAVING FIREBASE
    var thingArray = [String]()
    //let conditionRef = FIRDatabase.database().reference().child("condition")
    let arrayRef = FIRDatabase.database().reference()
    var refHandle = UInt()
    var userID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       if let user = FIRAuth.auth()?.currentUser {
            //user logged in
        
            userID = user.uid
            /*
            refHandle = arrayRef.observeEventType(FIRDataEventType.Value, withBlock: { (snapshot) in
                let dataDict = snapshot.value as! [String: AnyObject]
                //print(dataDict.count)
            })*/
            
        
            let userFireID: String = (FIRAuth.auth()?.currentUser?.uid)!

        /* This breaks if there isnt anything already in Firebase just needs an if statement*/

            arrayRef.child("Users").child(userFireID).observeSingleEvent(of: .value, with: { (snapshot) in
                let savedArray = snapshot.value!["Visits"] as! [String]
                self.thingArray=savedArray
                print(self.thingArray)
            })
        
        }

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /*This is creating a child into the root where we are going to store our data
        conditionRef.observeEventType(.Value) { (snap: FIRDataSnapshot) in
            //This is the label I'm updating in real time
            self.condition.text = snap.value?.description
        }*/
        
        arrayRef.observe(.value) { (snap: FIRDataSnapshot) in
            //This is the label I'm updating in real time
            //self.condition.text = snap.value?.description
        }
    }

    @IBAction func sunnyDidtouch(_ sender: AnyObject) {
        //SAVING FIREBASE
        //let userID = user.uid
        thingArray.append("sunny")
        self.arrayRef.child("Users").child(userID).child("Visits").setValue(thingArray)
        print(thingArray.count)
    }
    
    @IBAction func foggyDidtouch(_ sender: AnyObject) {
        thingArray.append("foggy")
        self.arrayRef.child("Users").child(userID).child("Visits").setValue(thingArray)
        print(thingArray)
    }
    
}

